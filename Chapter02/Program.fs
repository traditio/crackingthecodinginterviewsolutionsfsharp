﻿open System


type Node (d : int) = class
    let rec printToEnd (n : Node option) = 
            match n with 
            | None -> printfn "null"
            | Some n -> 
                printf "%u -> " n.Data
                printToEnd n.Next

    static let rec fromList (l : int list) (prev : Node) : Node =
        match l, prev with 
        | [], prev -> 
            prev.Next <- None
            prev
        | h :: t, prev ->
            let n = new Node(h)
            prev.Next <- Some n
            fromList t n
            
    static member FromList (l : int list) =        
        if l.Length < 1 then failwith "Can't create Node's linked list from empty int list"
        let x = new Node(List.head l)
        fromList (List.tail l) x |> ignore
        x

    member val Next : Node option = None with get, set
    member val Data = d with get, set

    override this.ToString () =
        let suffix = if this.Next.IsNone then "nil" else ""
        sprintf "%u" this.Data + suffix

    member this.PrintToEnd () = 
        printToEnd (Some this)
end


let Q_1_1 () = 
    // with no buffer
    let l = [ 1; 1; 2; 2; 3; 3 ]
    let first = Some <| Node.FromList l
    let mutable i = first
    let mutable j = i
    while j.IsSome do
        i <- j
        while j.IsSome && i.Value.Data = j.Value.Data do
            j <- j.Value.Next
        match j with
        | None -> i.Value.Next <- None
        | Some x -> i.Value.Next <- Some x       
    first.Value.PrintToEnd ()
    0


let Q_1_2 () = 
    // with buffer
    let l = [ 1; 1; 2; 2; ]
    let first = Some <| Node.FromList l
    let mutable i = first
    let mutable last : Node option = i
    let mutable set = Set.ofList [ first.Value.Data ]
    while i.IsSome do
        if not <| Set.contains i.Value.Data set then
            last.Value.Next <- i
            last <- i
            set <- Set.add i.Value.Data set
        else
            i <- i.Value.Next
    last.Value.Next <- None
    first.Value.PrintToEnd ()
    0


let Q_2 () = 
    let node = Node.FromList [ 0; 1; 2; 3; 4; 5 ]

    let lengthOfList (n : Node) = 
        let rec inner (m : Node option) acc = 
            match m with
            | None -> acc
            | Some m -> inner m.Next (acc + 1)
        inner (Some n) 0

    let findNthFromEnd (n : Node) k =
        let rec inner (m : Node) counter =
            match counter with
            | c when c > k -> inner m.Next.Value (counter - 1)
            | _ -> m
        inner n (lengthOfList n)
        
    printfn "%u" <| (findNthFromEnd node 3).Data
    0


let Q_3 () = 
    // delete node from the middle of list if access given only to that node (no access to the beginnig of list)
    let firstNode = Node.FromList [ 0; 1; 2; 3; 4; 5 ]

    let findNth (n : Node) k = 
        let rec inner (m : Node) counter = 
            match counter with
            | x when x < k -> inner m.Next.Value (counter + 1)
            | _ -> m
        inner n 0

    firstNode.PrintToEnd ()

    let node = findNth firstNode 2
    // delete third node
    if node.Next.IsSome then
        node.Data <- node.Next.Value.Data
        node.Next <- node.Next.Value.Next

    firstNode.PrintToEnd ()
    0

[<EntryPoint>]
let main argv = 
    printfn "%A" <| Q_3 () // change the question number here
    Console.ReadKey() |> ignore
    0 // возвращение целочисленного кода выхода
