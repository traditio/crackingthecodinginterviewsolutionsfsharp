﻿// Learn more about F# at http://fsharp.net
// See the 'F# Tutorial' project for more help.

// Determine if all symbols in string are unique
open System.Collections
open System.Text
open System


let Q_1 () = 
    let rec areAllUnique (chars : byte list) (flags : int) : bool =
        // 1. use array of bool[all ASCII charecters size]
        // 2. use list of chars
        // 3. use dictionary
        // 4. use bitarray
        // 5. use int
        match chars with
        | h :: t ->
            if (flags &&& (1 <<< (int32 h))) > 0 then
                false
            else
                areAllUnique t (flags ||| (1 <<< (int32 h)))
        | _ -> true

    //let string = "qwertyuiopasdfghjklzxcvbnmq"
    let string = "asdf"
    let bytes = Array.toList <| ASCIIEncoding.ASCII.GetBytes(string)
    let isNotAscii = List.tryFind (fun b -> b = (byte 63)) bytes
    if isNotAscii.IsSome then failwith "Not ASCII"    
    areAllUnique bytes 0    
    

let Q_2_1 () = 
    let rec reverse (s : char list) = 
        match s with        
        | h :: t -> List.append (reverse t) [ h ]
        | _ ->  []
    let listOfChars = Array.toList <| "abcde".ToCharArray()
    let reversed = List.toArray <| reverse listOfChars
    new String(reversed)
    

let Q_2_2 () = 
    let reverse (s : string) = 
        let sb = new StringBuilder()
        let mutable i = 0
        let mutable j = s.Length - 1
        while j >= i do
            let pos = sb.Length / 2
            sb.Insert(pos, s.[i]) |> ignore
            if i <> j then
                sb.Insert(pos, s.[j]) |> ignore
            i <- i + 1  
            j <- j - 1
        sb.ToString()
    [ reverse "abcd" ; reverse "abcde" ]
       

let Q_3 () = 
    let isPermutation (s1 : string) (s2 : string) = 
        // 1. sort and compare
        // 2. count the symbols count
        // 2.1 group (linq) and count, compare groups
        if s1.Length <> s2.Length then 
            false
        else
            let charCount1 = Array.create 256 0
            for c in s1 do
                charCount1.[int32 c] <- charCount1.[int32 c] + 1            
            for c in s2 do
                charCount1.[int32 c] <- charCount1.[int32 c] - 1
            not <| query { for c in charCount1 do exists (c > 0) }
    [ isPermutation "abcd" "bcda" ; isPermutation "abcda" "abcdd" ]


let Q_4 () = 
    let s = "ab cd e"
    let mutable spacesCount = 0
    for c in s do if c = ' ' then spacesCount <- spacesCount + 1
    let s2 = Array.create (s.Length + spacesCount * 2) ' '
    let mutable cursor = s2.Length - 1
    for i = (s.Length - 1) downto 0 do
        if s.[i] <> ' ' then
            s2.[cursor] <- s.[i]
            cursor <- cursor - 1
        else
            s2.[cursor] <- '0'
            s2.[cursor - 1] <-  '2'
            s2.[cursor - 2] <-  '%'
            cursor <- cursor - 3
    new String(s2)


let Q_5 () = 
    //let s = "aaabcccccaa"
    let s = "aa"
    let res = new StringBuilder()
    let mutable counter = 1
    for i = 1 to s.Length - 1 do
        if s.[i] = s.[i-1] then
            counter <- counter + 1
        else
            res.Append(s.[i-1]) |> ignore
            res.Append(counter) |> ignore
            counter <- 1

    res.Append(s.[s.Length - 1]) |> ignore
    res.Append(counter) |> ignore

    if res.Length > s.Length then
        s
    else
        res.ToString()


let Q_6 () = 
    let matrix = array2D [| [| 1; 2; 3 |]; [| 4; 5; 6 |]; [| 7; 8; 9|]; [| 10; 11; 12|] |]
    let rows = Array2D.length1 matrix
    let cols = Array2D.length2 matrix
   
    let newMatrix = Array2D.create cols rows 0
    for i = 0 to rows - 1 do
        for j = 0 to cols - 1 do
            newMatrix.[j, rows - 1 - i] <- matrix.[i, j]

    for i = 0 to (Array2D.length1 newMatrix - 1) do
        for j = 0 to (Array2D.length2 newMatrix - 1) do
            printf "%-5u" newMatrix.[i, j]
        printfn ""


let Q_7 () =
    let matrix = [| 0; 1; 2; 3; 4; 5; 6; 7; 8; 9; 10; 11 |]
    let rows = 3
    let cols = 4
    let nulledRows = ref List.empty<int>
    let nulledCols = ref List.empty<int>

    let idx row col : int = 
        row * cols + col
    
    let nullabe row col = 
        nulledCols := List.append !nulledCols [ col ]
        nulledRows := List.append !nulledRows [ row ]

        // null column
        for i = 0 to rows - 1 do
            matrix.[idx i col] <- 0
            
        // null row
        for i = 0 to cols - 1 do
            matrix.[idx row i] <- 0
            
    let rec innerLoop n m = 
        match n, m with
        | n, m when m < cols -> 
            if not ((List.exists ((=) n) !nulledRows) || (List.exists ((=) m) !nulledCols)) then
                if matrix.[idx n m] = 0 then nullabe n m
            innerLoop n (m + 1)
        | _, _ -> ()

    let rec outerLoop n =         
        match n with
        | n when n < rows ->
            if not (List.exists ((=) n) !nulledRows) then innerLoop n 0
        | _ -> ()

    outerLoop 0

    for i = 0 to rows - 1 do
        for j = 0 to cols - 1 do
            printf "%-5u" <| matrix.[idx i j]
        printfn "" |> ignore


let Q_8 () =
    let isSubstring (str : string) (substr : string) = 
        str.Contains substr 
    let s = "erbottlewat"
    let subs = "waterbottle"
    isSubstring (s + s) subs
        

[<EntryPoint>] 
let main argv = 
    printfn "%A" <| Q_8 () // change the question number here
    Console.ReadKey() |> ignore
    0 // return an integer exit code